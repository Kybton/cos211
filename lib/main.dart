import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

void main() {
  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'ThuraKhantThein',
    theme: ThemeData(
      primarySwatch: Colors.blue,
    ),
    home: LoginPage(),
  ));
}

class LoginPage extends StatelessWidget {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  LoginPage({super.key});

  void login() {
    String username = usernameController.text;
    String password = passwordController.text;

    if (username.isNotEmpty && password.isNotEmpty) {
      Get.off(const HomePage());
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Rental',
            ),
            const SizedBox(height: 20),
            TextField(
              controller: usernameController,
              decoration: InputDecoration(
                labelText: 'Username',
              ),
            ),
            SizedBox(height: 20),
            TextField(
              controller: passwordController,
              obscureText: true,
              decoration: InputDecoration(
                labelText: 'Password',
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: login,
              child: Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}

class Bedroom {
  final String referenceName;
  final String propertyType;
  final String bedRoom;
  final String createTime;
  final String price;
  final String funType;
  final String note;

  Bedroom({
    required this.price,
    required this.funType,
    required this.bedRoom,
    required this.createTime,
    required this.propertyType,
    required this.referenceName,
    required this.note,
  });
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<Bedroom> bedrooms = [
    Bedroom(
        price: '100',
        funType: 'T',
        bedRoom: '1',
        createTime: '11:00',
        propertyType: 'propertyType',
        referenceName: 'referenceName',
        note: 'note'),
    Bedroom(
        price: '100',
        funType: 'T',
        bedRoom: '1',
        createTime: '11:00',
        propertyType: 'propertyType',
        referenceName: 'referenceName',
        note: 'note'),
    Bedroom(
        price: '100',
        funType: 'T',
        bedRoom: '1',
        createTime: '11:00',
        propertyType: 'propertyType',
        referenceName: 'referenceName',
        note: 'note')
  ];
  final TextEditingController priceC = TextEditingController();
  final TextEditingController funC = TextEditingController();
  final TextEditingController bedC = TextEditingController();
  final TextEditingController createC = TextEditingController();
  final TextEditingController properC = TextEditingController();
  final TextEditingController referC = TextEditingController();
  final TextEditingController noteC = TextEditingController();

  void editBedroom(int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Edit Bedroom'),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  controller: priceC,
                  decoration: InputDecoration(
                    labelText: 'Price',
                  ),
                ),
                SizedBox(height: 16),
                TextField(
                  controller: funC,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Furniture Type',
                  ),
                ),
                TextField(
                  controller: bedC,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Bedroom',
                  ),
                ),
                TextField(
                  controller: createC,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Create Time',
                  ),
                ),
                TextField(
                  controller: properC,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Property Type',
                  ),
                ),
                TextField(
                  controller: referC,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Reference',
                  ),
                ),
                TextField(
                  controller: noteC,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Note',
                  ),
                ),
              ],
            ),
          ),
          actions: [
            ElevatedButton(
              onPressed: () {
                String price = priceC.text;
                String fun = funC.text;
                String bed = bedC.text;
                String create = createC.text;
                String proper = properC.text;
                String refer = referC.text;
                String note = noteC.text;

                // Update the bedroom details in the list
                setState(() {
                  bedrooms[index] = Bedroom(
                      price: price,
                      funType: fun,
                      bedRoom: bed,
                      createTime: create,
                      propertyType: proper,
                      referenceName: refer,
                      note: note);
                });

                priceC.clear();
                funC.clear();
                bedC.clear();
                createC.clear();
                properC.clear();
                referC.clear();
                noteC.clear();
                Navigator.pop(context);
              },
              child: Text('Save'),
            ),
          ],
        );
      },
    );
  }

  void createCard() {
    String price = priceC.text;
    String fun = funC.text;
    String bed = bedC.text;
    String create = createC.text;
    String proper = properC.text;
    String refer = referC.text;
    String note = noteC.text;

    Bedroom newBedroom = Bedroom(
      price: price,
      funType: fun,
      bedRoom: bed,
      createTime: create,
      propertyType: proper,
      referenceName: refer,
      note: note,
    );

    setState(() {
      bedrooms.add(newBedroom);
    });

    priceC.clear();
    funC.clear();
    bedC.clear();
    createC.clear();
    properC.clear();
    referC.clear();
    noteC.clear();
    Navigator.pop(context);
  }

  void deleteBedroom(int index) {
    setState(() {
      bedrooms.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        itemCount: bedrooms.length,
        itemBuilder: (context, index) {
          Bedroom bedroom = bedrooms[index];
          return Card(
            child: ListTile(
              title: Text(bedroom.referenceName),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Bedroom: ${bedroom.bedRoom}'),
                  Text('Note: ${bedroom.note}'),
                  Text('Furniture Type: ${bedroom.funType}'),
                  Text('Create Time: ${bedroom.createTime}'),
                  Text('Price: ${bedroom.price}'),
                  Text('Property Type: ${bedroom.propertyType}'),
                ],
              ),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    onPressed: () => editBedroom(index),
                    icon: Icon(Icons.edit),
                  ),
                  IconButton(
                    onPressed: () => deleteBedroom(index),
                    icon: Icon(Icons.delete),
                  ),
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text('Create New Card'),
                content: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextField(
                        controller: priceC,
                        decoration: const InputDecoration(
                          labelText: 'Bedroom Name',
                        ),
                      ),
                      const SizedBox(height: 16),
                      TextField(
                        controller: funC,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          labelText: 'Furniture Type',
                        ),
                      ),
                      TextField(
                        controller: bedC,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Bedroom',
                        ),
                      ),
                      TextField(
                        controller: createC,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Create Time',
                        ),
                      ),
                      TextField(
                        controller: properC,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Property Type',
                        ),
                      ),
                      TextField(
                        controller: referC,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Reference Type',
                        ),
                      ),
                      TextField(
                        controller: noteC,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Note',
                        ),
                      ),
                    ],
                  ),
                ),
                actions: [
                  ElevatedButton(
                    onPressed: () {
                      createCard();
                    },
                    child: Text('Create'),
                  ),
                ],
              );
            },
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
